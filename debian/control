Source: debmake-doc
Section: doc
Priority: optional
Maintainer: Osamu Aoki <osamu@debian.org>
Build-Depends: asciidoc,
               dblatex,
               debhelper-compat (= 13),
               dh-python,
               docbook-xml,
               docbook-xsl,
               fonts-arphic-bsmi00lp,
               fonts-arphic-gbsn00lp,
               fonts-ipafont,
               fonts-liberation,
               fonts-wqy-zenhei,
	       po4a,
               latex-cjk-all,
               libxml2-utils,
               lmodern,
               locales-all | locales,
               texlive,
               texlive-lang-chinese,
               texlive-lang-cyrillic,
               texlive-lang-english,
               texlive-lang-french,
               texlive-lang-german,
               texlive-lang-japanese,
               texlive-xetex,
               xsltproc,
	       w3m,
               zip
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://salsa.debian.org/debian/debmake-doc
Vcs-Git: https://salsa.debian.org/debian/debmake-doc.git
Vcs-Browser: https://salsa.debian.org/debian/debmake-doc

Package: debmake-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: debmake
Suggests: debian-policy, developers-reference
Description: Guide for Debian Maintainers
 This tutorial document describes the building of the Debian
 package to ordinary Debian users and prospective developers using
 the debmake command.
 .
 It is focused on the modern packaging style and comes with many
 simple examples.
  * POSIX shell script packaging
  * Python3 script packaging
  * C with Makefile/Autotools/CMake
  * multiple binary package with shared library etc.
 .
 This package can be considered as the successor to the
 maint-guide package.
