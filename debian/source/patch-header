The Debian packaging of debmake-doc is maintained in git, using the merging
workflow described in dgit-maint-merge(7).  There isn't a patch queue
that can be represented as a quilt series.

Currently, debmake-doc is hosted at https://salsa.debian.org/debian/debmake-doc
and this is the best place to see change history.

Seeing the git history in the above repository should give you good
clear perspective what is in the single combined diff of this package.

Once this package is uploaded with dgit, a detailed breakdown of the changes
will available from their canonical representation - git commits in the
packaging repository.  For example, to see the changes made by the Debian
maintainer in the first upload of upstream version 1.2.3, you could use:

    % git clone https://git.dgit.debian.org/debmake-doc
    % cd debmake-doc
    % git log --oneline 1.2.3..debian/1.2.3-1 -- . ':!debian'

(If you have dgit, use `dgit clone foo`, rather than plain `git clone`.)

A single combined diff, containing all the changes, follows.

