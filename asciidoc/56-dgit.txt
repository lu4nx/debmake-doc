[[dgit]]
=== dgit

The new *dgit* package offers commands to automate packaging activities using the git repository as an alternative to still popular *gbp-buildpackage*.  Please read their guide:

* *dgit-maint-merge*(7) -- for dpkg-source "`Format: 3.0 (quilt)`" package with its changes flowing both ways between the upstream Git repository and the Debian Git repository which are tightly coupled.
* *dgit-maint-debrebase*(7) -- for dpkg-source "`Format: 3.0 (quilt)`" package with its changes flowing mostly one way from the upstream Git repository to the Debian Git repository.
* *dgit-maint-gbp*(7) -- for dpkg-source "`Format: 3.0 (quilt)`" package with its Debian Git repository which is kept usable also for people using *gbp-buildpackage*(1).
* *dgit-maint-native*(7) -- for dpkg-source "`Format: 3.0 (native)`" package in the Debian Git repository.

The *dgit*(1) command can push the easy-to-trace change history to the https://browse.dgit.debian.org/ site and can upload Debian package to the Debian repository properly without using *dput*(1).

* *dgit push-source* -- for uploading source-only package (normal)
* *dgit push* -- for uploading binary package

If you already use `main` and `upstream` branches in the Debian packaging repository (`salsa.debian.org`), you can use your local `upstream-vcs-main` branch to track the `main` branch of the `upstream-vcs` repository as:

----
 $ git fetch upstream-vcs main:upstream-vcs-main
----

Cherry picking bug fix commits from latest upstream commits are intuitive operation with *dgit-maint-merge*(7) and *dgit-maint-debrebase*(7).  Just right-click those commits interactively on the `upstream-vcs-main` branch of the *gitk*(1) GUI dialog.

[[deborig]]
==== Snapshot upstream tarball (alternative `git deborig` approach)

For *quasi-native* Debian binary package scheme situation described in <<dt-option>> can be addressed using alternative `git deborig` approach adopting the *dgit-maint-merge*(7) scheme when `debian/changelog` contains the non-native version number with revision like `0.16-1`.

----
 $ cd /path/to/upstream-git
 $ git rm -rf debian
 $ git tag -s upstream/0.16
 $ git reset --hard HEAD^
 $ git deborig
 $ sbuild
----

Here, for source format `3.0 (quilt)`, removal of files under `debian/` directory in the upstream tarball is non-essential operation to quiet false positive warning from lintian.

For `-1` revision, this use of `git-deborig`(1) as above is how this `debmake-doc` package generates the upstream tarball.

For `-2`, `-3`, ... revisions, you need to fetch and use the uploaded upstream tarball instead.  For this, `origtargz`(1) may be handy.

