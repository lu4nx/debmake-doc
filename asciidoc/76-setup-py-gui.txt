[[setup-py-gui]]
=== setup.py (Python3, GUI)

Here is an example of creating a simple Debian package from a Python3 GUI program using the *setuptools* based *setup.py* as its build system.

This upstream is based on <<setup-py>> with enhanced GUI, desktop icon, and manpage support.

Let's assume this upstream tarball to be *debhello-1.3.tar.gz*.

Let's get the source and make the Debian package.

.Download *debhello-1.3.tar.gz*
----
include::../examples/debhello-1.3_build-1/step000.slog[]
----

Here, the content of this *debhello* source tree as follows.

.*setup.py* (v=1.3) -- a boilar plate *setup.py* for the backward compatibility
----
include::../examples/debhello-1.3_build-1/step101.slog[]
----

.*setup.cfg* (v=1.3) -- modern static metadata
----
include::../examples/debhello-1.3_build-1/step102.slog[]
----

.*pyproject.toml* (v=1.3) -- https://www.python.org/dev/peps/pep-0517/[PEP 517] configuration
----
include::../examples/debhello-1.3_build-1/step103.slog[]
----

.*MANIFEST.in* (v=1.3) -- for *distutils* to make *sdist*
----
include::../examples/debhello-1.3_build-1/step104.slog[]
----

.*src/debhello/\_\_init__.py* (v=1.3) -- program version definition
----
include::../examples/debhello-1.3_build-1/step105.slog[]
----

.*src/debhello/\_\_main__.py* (v=1.3) -- command entry point
----
include::../examples/debhello-1.3_build-1/step106.slog[]
----

Let's package this with the *debmake* command.  Here, the *-b\':py3'* option is used to specify that the generated binary package contains Python3 script and module files.

----
sys::[head -n3 ../examples/debhello-1.3_build-1/step200.slog]
 ...
sys::[grep -A12  '^I: sanity check of parameters' ../examples/debhello-1.3_build-1/step200.slog]
 ...
----

The result is practically the same as in <<setup-py>>.

Let's make this Debian package better as the maintainer.

.*debian/rules* (maintainer version, v=1.3):
----
include::../examples/debhello-1.3_build-1/step301.slog[]
----

.*debian/control* (maintainer version, v=1.3):
----
include::../examples/debhello-1.3_build-1/step302.slog[]
----

Please note the manually added *python3-gi* and *gir1.2-gtk-3.0* dependencies.

The rest of the packaging activities are practically the same as in <<setup-py>>.

Here is the generated dependency list of *debhello_1.3-1_all.deb*.

.The generated dependency list of *debhello_1.3-1_all.deb*:
----
include::../examples/debhello-1.3_build-1/step702.slog[]
----

