[[details]]
=== Details

Actual details of the examples presented and their variants can be obtained by the following.

.How to get details
----
 $ apt-get source debmake-doc
 $ cd debmake-doc*
 $ cd examples
 $ view examples/README.md
----

Follow the exact instruction in `examples/README.md`.

----
 $ cd examples
 $ sudo apt-get install devscripts build-essentials ...
 $ make
----

Now, each directory named as `examples/debhello-?.?_build-?` contains the Debian packaging example.

* emulated console command line activity log: the *.log* file
* emulated console command line activity log (short): the *.slog* file
* snapshot source tree image after the *debmake* command: the *debmake* directory
* snapshot source tree image after proper packaging: the *packge* directory
* snapshot source tree image after the *debuild* command: the *test* directory

Notable examples are:

* the Python3 script with bare distutils (v=1.8),
* the POSIX shell script with Makefile with i18n (v=3.0),
* the Python3 script with setuptools with i18n (v=3.1),
* the C source with Makefile.in + configure with i18n (v=3.2),
* the C source with Autotools with i18n with i18n (v=3.3), and
* the C source with CMake with i18n (v=3.4).


